package edu.upc.gco.slcnt.orch.nmro;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import edu.upc.gco.slcnt.rest.client.osm.OSMr4PlusClientExtended;
import edu.upc.gco.slcnt.rest.client.osm.lcm.OSMLcmClient;
import edu.upc.gco.slcnt.rest.client.osm.model.nsiManagement.NSInstance;
import edu.upc.gco.slcnt.rest.client.osm.model.osmManagement.VimInfoObject;
import edu.upc.gco.slcnt.rest.client.osm2.OsmApi;
//import it.nextworks.nfvmano.libs.descriptors.nsd.nodes.NsVirtualLink.NsVirtualLinkNode;
import it.nextworks.nfvmano.libs.osmr4PlusClient.OSMr4PlusClient;
import it.nextworks.nfvmano.libs.osmr4PlusClient.utilities.OSMHttpResponse;
import it.nextworks.nfvmano.libs.osmr4PlusDataModel.nsDescriptor.ConstituentVNFD;
import it.nextworks.nfvmano.libs.osmr4PlusDataModel.nsDescriptor.VLD;
import it.nextworks.nfvmano.libs.osmr4PlusDataModel.nsDescriptor.VNFDConnectionPointReference;
import it.nextworks.nfvmano.libs.osmr4PlusDataModel.osmManagement.OsmInfoObject;

/**
 * NMR-O Driver Application for testing
 *
 */
public class DriverApp 
{
	enum LcmOp
	{
		createNSId,
		deleteNSInstance;
	}
	
    public static void main1( String[] args )
    {
        System.out.println( "Hello World!" );
        OsmApi osm = new OsmApi("10.202.1.31", "9999", "admin","admin","admin");
        /*
        TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

        SSLContext sslContext = null;
		try {
			sslContext = org.apache.http.ssl.SSLContexts.custom()
			        .loadTrustMaterial(null, acceptingTrustStrategy)
			        .build();
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);

        CloseableHttpClient httpClient = HttpClients.custom()
                .setSSLSocketFactory(csf)
                .build();

        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();

        requestFactory.setHttpClient(httpClient);

        RestTemplate restTemplate = new RestTemplate(requestFactory);
        */
        String result = osm.getAdminToken(/*restTemplate, "https://10.202.1.31:9999/osm/admin/v1/tokens", "admin","admin","admin"*/);
        System.out.println("Token: " + result);
        //osm.listTokens(/*restTemplate, "https://10.202.1.31:9999/osm/admin/v1/tokens", result*/);
        osm.listNSDescriptors();
    }
    
    private static <T> List<T> parseResponse(OSMHttpResponse httpResponse, String opId, Class<T> clazz) {
        List<T> objList = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
        mapper.enable(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT);
        mapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);

        if ((httpResponse.getCode() >= 300) || (httpResponse.getCode() < 200)) {
        	System.out.println(System.out.format("Unexpected response code {}: {}. OpId: {}", httpResponse.getCode(), httpResponse.getMessage(), opId).toString());
        	System.out.println(System.out.format("Response content: {}", httpResponse.getContent()).toString());
        }
        // else, code is 2XX
        if (clazz == null)
            return null;
        if (httpResponse.getFilePath() != null)
            objList.add(clazz.cast(httpResponse.getFilePath().toFile()));
        else
            try {
                Class<T[]> arrayClass = (Class<T[]>) Class.forName("[L" + clazz.getName() + ";");
            	objList = Arrays.asList(mapper.readValue(httpResponse.getContent(), arrayClass));
            } catch (Exception e) {
            	System.out.println("Could not obtain objects from response: " + e.getMessage());
            }
        return objList;
    }
    
    private static List<VLD> parseResponse(String response) {
        List<VLD> objList = null;
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
        mapper.enable(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT);
        mapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        //VLD t = null;

        try {
        	objList = mapper.readValue(response, new TypeReference<List<VLD>>() {});
        	//Class<T[]> arrayClass = (Class<T[]>) Class.forName("[L" + clazz.getName() + ";");
        	//objList = Arrays.asList(mapper.readValue(response, arrayClass));
        } catch (Exception e) {
        	System.out.println("Could not obtain objects from response: " + e.getMessage());
        }
        return objList;
    }
    
    private VNFDConnectionPointReference makeCPRef(String vnfdId, int memberIndex, String cpId) {
        VNFDConnectionPointReference cpr = new VNFDConnectionPointReference();
        cpr.setVnfdIdReference(vnfdId);
        cpr.setIndexReference(memberIndex);
        cpr.setVnfdConnectionPointReference(cpId);
        return cpr;
    }
    
    /*private VLD makeVld(
            String vlId,
            NsVirtualLinkNode link,
            Map<String, Map<ConstituentVNFD, String>> vlToVnfMapping
    ) {
        VLD vld = new VLD();
        vld.setId(vlId);
        vld.setName(vlId);
        vld.setShortName(vlId);
        vld.setMgmtNetwork(vlId.endsWith("_mgmt") || vlId.startsWith("mgmt_") || vlId.equalsIgnoreCase("default")); // TODO
        vld.setType("ELAN");
        vld.setVimNetworkName(vlId);
        vld.setVnfdConnectionPointReferences(
                vlToVnfMapping.getOrDefault(vlId, Collections.emptyMap()).entrySet()
                        .stream()
                        .map(e -> makeCPRef(
                                e.getKey().getVnfdIdentifierReference(),
                                e.getKey().getMemberVNFIndex(),
                                e.getValue()
                        ))
                        .collect(Collectors.toList())
        );

        return vld;
    }*/
    
    public static void main2( String[] args )
    {
    	System.out.println( "OSMr4Plus Client test" );
    	OSMr4PlusClientExtended osmClient = new OSMr4PlusClientExtended("10.202.1.31", "9999", "admin","admin","admin");
    	
    	OSMHttpResponse response = osmClient.getNsdInfoList();
    	//OSMHttpResponse response = osmClient.getNsiInfoList();
    	//response = osmClient.getNsdContent("9b198fb8-8958-43a5-bf99-963e793c7274", "/home/fernando/");
    	//System.out.println("Response= " + response.getCode());
    	
    	
    	List<OsmInfoObject> aux = new ArrayList<>();
    	aux = parseResponse(response, null, OsmInfoObject.class);
    	for (OsmInfoObject o : aux)
    	{
    		//Map<String,Object> m = o.any();
    		//m.forEach((key, value) -> System.out.println(key + " = " + value + " / " + value.getClass()));
    		System.out.println("OsmInfoObject = " + o.toString());
    	}
    }
    
    private static void testOsmInventory (OSMLcmClient osmClient)
    {
    	ResponseEntity<List<NSInstance>> re = (ResponseEntity<List<NSInstance>>) osmClient.getNsiInfoList();
    	List<NSInstance> nsiList;
    	nsiList = (List<NSInstance>) re.getBody();
		for (NSInstance nsi : nsiList)
		{
			ResponseEntity<NSInstance> rei = (ResponseEntity<NSInstance>) osmClient.getNsiInfo(nsi.getId());
			NSInstance nsi1 = (NSInstance) rei.getBody(); 
			System.out.println("RESPONSE: " + nsi1.toString());
		}
    }
    
    private static void testOsmLcm (OSMLcmClient osmClient, LcmOp operation)
    {
    	switch (operation)
    	{
    		case createNSId:
    		{
    			String nsName = "TestUPC";
    			String nsdId = "7a250664-1289-405b-9f42-e1ec4cbad356";
    			String nsDescription = "Test from NMR-O Driver";
    			String vimAccountId = "6221b8b2-a737-4a94-87f9-5c1b13e3a6e7";
    			ResponseEntity<String> re = (ResponseEntity<String>) osmClient.createNsiIdentifier(nsName, nsdId, nsDescription, vimAccountId);
    			System.out.println("Response: " + re.getBody());
    			break;
    		}
    		case deleteNSInstance:
    		{
    			String nsId = "1c0b5968-2aa4-4619-b991-18de406629af";
    			ResponseEntity<Void> re = (ResponseEntity<Void>) osmClient.deleteNsInstance(nsId);
    			System.out.println("Response Status: " + re.getStatusCodeValue());
    			break;
    		}
    		default:
    			break;
    	}
    }
    
    private static void testOsmManagement (OSMLcmClient osmClient)
    {
    	ResponseEntity<List<VimInfoObject>> re = (ResponseEntity<List<VimInfoObject>>) osmClient.getVimInfoList();
    	List<VimInfoObject> vimList;
    	vimList = (List<VimInfoObject>) re.getBody();
		for (VimInfoObject vim : vimList)
		{
			ResponseEntity<VimInfoObject> rev = (ResponseEntity<VimInfoObject>) osmClient.getVimInfo(vim.getId());
			VimInfoObject vimi = (VimInfoObject) rev.getBody(); 
			System.out.println("RESPONSE: " + vimi.toString());
		}
    }
    
    public static void main( String[] args)
    {
    	System.out.println( "OSM LCM Client test" );
    	
    	OSMLcmClient osmClient = new OSMLcmClient("10.202.1.31", "admin","admin","admin");
    	//testOsmManagement (osmClient);
    	//testOsmInventory (osmClient);
    	//testOsmLcm (osmClient, LcmOp.createNSId);
    	testOsmLcm (osmClient, LcmOp.deleteNSInstance);
    }
}
