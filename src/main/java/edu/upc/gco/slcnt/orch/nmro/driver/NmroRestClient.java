package edu.upc.gco.slcnt.orch.nmro.driver;


import it.nextworks.nfvmano.libs.ifa.osmanfvo.nslcm.interfaces.messages.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.messages.QueryNsdResponse;
import it.nextworks.nfvmano.libs.ifa.catalogues.interfaces.messages.QueryOnBoardedVnfPkgInfoResponse;
import it.nextworks.nfvmano.libs.ifa.common.enums.OperationStatus;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.FailedOperationException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MethodNotImplementedException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.NotExistingEntityException;
import it.nextworks.nfvmano.libs.ifa.common.messages.GeneralizedQueryRequest;



/**
 * REST client to interact with NMR-O.
 * 
 * @author UPC based on Nextworks 
 *
 */
public class NmroRestClient {

	private static final Logger log = LoggerFactory.getLogger(NmroRestClient.class);
	
	private RestTemplate restTemplate;

	private String nsLifecycleServiceUrl;
	
	private String nsdCatalogueServiceUrl;
	
	private String vnfdCatalogueServiceUrl;
	
	/**
	 * Constructor
	 * 
	 * @param nmroUrl root URL to interact with NMR-O
	 * @param restTemplate REST template
	 */
	public NmroRestClient(String nmroUrl, RestTemplate restTemplate) {

		this.nsLifecycleServiceUrl = nmroUrl + "/nfvo/nsLifecycle";
		this.nsdCatalogueServiceUrl = nmroUrl + "/nfvo/nsdManagement";
		this.vnfdCatalogueServiceUrl = nmroUrl + "/nfvo/vnfdManagement";
		this.restTemplate = restTemplate;
	}
	
	
	//******************************** NS LCM methods ********************************//
	
	public String createNsIdentifier(CreateNsIdentifierRequest request) throws NotExistingEntityException, FailedOperationException, MalformattedElementException {

		log.debug("Building HTTP request to create NS ID.");
		HttpHeaders header = new HttpHeaders();
		header.add("Content-Type", "application/json");
		HttpEntity<?> postEntity = new HttpEntity<>(request, header);

		String url = nsLifecycleServiceUrl + "/ns";

		try {
			log.debug("Sending HTTP request to create NS ID.");
			ResponseEntity<String> httpResponse = 
    				restTemplate.exchange(url, HttpMethod.POST, postEntity, String.class);
			
			log.debug("Response code: " + httpResponse.getStatusCode().toString());
			HttpStatus code = httpResponse.getStatusCode();
			
			if (code.equals(HttpStatus.CREATED)) {
				String nsId = httpResponse.getBody();
				log.debug("Created NS ID at NFVO: " + nsId);
				return nsId;
			} else if (code.equals(HttpStatus.NOT_FOUND)) {
				throw new NotExistingEntityException("Error during NS ID creation at NFVO: " + httpResponse.getBody());
			} else if (code.equals(HttpStatus.BAD_REQUEST)) {
				throw new MalformattedElementException("Error during NS ID creation at NFVO: " + httpResponse.getBody());
			} else {
				throw new FailedOperationException("Generic error on NFVO during NS ID creation");
			}
				
		} catch (RestClientException ex) {
			log.debug("Error while interacting with NFVO.");
			throw new FailedOperationException("Error while interacting with NFVO LCM at url " + url);
		}
	}

	public String instantiateNs(InstantiateNsRequest request) throws NotExistingEntityException, FailedOperationException, MalformattedElementException {
		String nsId = request.getNsInstanceId();
		log.debug("Building HTTP request to instantiate NS instance " + nsId);
		
		HttpHeaders header = new HttpHeaders();
		header.add("Content-Type", "application/json");
		HttpEntity<?> putEntity = new HttpEntity<>(request, header);

		String url = nsLifecycleServiceUrl + "/ns/" + nsId + "/instantiate";
		
		try {
			log.debug("Sending HTTP request to instantiate NS.");
			ResponseEntity<String> httpResponse =
					restTemplate.exchange(url, HttpMethod.PUT, putEntity, String.class);
			
			log.debug("Response code: " + httpResponse.getStatusCode().toString());
			HttpStatus code = httpResponse.getStatusCode();
			
			if (code.equals(HttpStatus.OK)) {
				String operationId = httpResponse.getBody();
				log.debug("Started NS instantiation at NFVO. Operation ID: " + operationId);
				return operationId;
			} else if (code.equals(HttpStatus.NOT_FOUND)) {
				throw new NotExistingEntityException("Error during NS instantiation at NFVO: " + httpResponse.getBody());
			} else if (code.equals(HttpStatus.BAD_REQUEST)) {
				throw new MalformattedElementException("Error during NS instantiation at NFVO: " + httpResponse.getBody());
			} else {
				throw new FailedOperationException("Generic error on NFVO during NS instantiation");
			}
			
		} catch (RestClientException e) {
			log.debug("Error while interacting with NFVO.");
			throw new FailedOperationException("Error while interacting with NFVO LCM at url " + url);
		}
	}

	public String scaleNs(ScaleNsRequest request) throws NotExistingEntityException, FailedOperationException, MalformattedElementException {
		String nsId = request.getNsInstanceId();
		log.debug("Building HTTP request to scale NS instance " + nsId);

		HttpHeaders header = new HttpHeaders();
		header.add("Content-Type", "application/json");
		HttpEntity<?> putEntity = new HttpEntity<>(request, header);

		String url = nsLifecycleServiceUrl + "/ns/" + nsId + "/scale";

		try {
			log.debug("Sending HTTP request to instantiate NS.");
			ResponseEntity<String> httpResponse =
					restTemplate.exchange(url, HttpMethod.PUT, putEntity, String.class);

			log.debug("Response code: " + httpResponse.getStatusCode().toString());
			HttpStatus code = httpResponse.getStatusCode();

			if (code.equals(HttpStatus.OK)) {
				String operationId = httpResponse.getBody();
				log.debug("Started NS instantiation at NFVO. Operation ID: " + operationId);
				return operationId;
			} else if (code.equals(HttpStatus.NOT_FOUND)) {
				throw new NotExistingEntityException("Error during NS instantiation at NFVO: " + httpResponse.getBody());
			} else if (code.equals(HttpStatus.BAD_REQUEST)) {
				throw new MalformattedElementException("Error during NS instantiation at NFVO: " + httpResponse.getBody());
			} else {
				throw new FailedOperationException("Generic error on NFVO during NS instantiation");
			}

		} catch (RestClientException e) {
			log.debug("Error while interacting with NFVO.");
			throw new FailedOperationException("Error while interacting with NFVO LCM at url " + url);
		}
	}
	
	public QueryNsResponse queryNs(String nsInstanceId) throws NotExistingEntityException, FailedOperationException, MalformattedElementException {
		log.debug("Building HTTP request for querying NS instance with ID " + nsInstanceId);
		HttpHeaders header = new HttpHeaders();
		header.add("Content-Type", "application/json");
		HttpEntity<?> getEntity = new HttpEntity<>(header);
		
		String url = nsLifecycleServiceUrl + "/ns/" + nsInstanceId;
		
		try {
			log.debug("Sending HTTP request to retrieve NS instance.");
			
			ResponseEntity<QueryNsResponse> httpResponse =
					restTemplate.exchange(url, HttpMethod.GET, getEntity, QueryNsResponse.class);
			
			log.debug("Response code: " + httpResponse.getStatusCode().toString());
			HttpStatus code = httpResponse.getStatusCode();
			
			if (code.equals(HttpStatus.OK)) {
				log.debug("NS instance correctly retrieved");
				return httpResponse.getBody();
			} else if (code.equals(HttpStatus.NOT_FOUND)) {
				throw new NotExistingEntityException("Error during NS retrieval at NFVO: " + httpResponse.getBody());
			} else if (code.equals(HttpStatus.BAD_REQUEST)) {
				throw new MalformattedElementException("Error during NS retrieval at NFVO: " + httpResponse.getBody());
			} else {
				throw new FailedOperationException("Generic error on NFVO during NS retrieval");
			}
			
		} catch (Exception e) {
			log.debug("Error while interacting with NFVO.");
			throw new FailedOperationException("Error while interacting with NFVO LCM at url " + url);
		}
	}
	
	public String terminateNs(TerminateNsRequest request) throws NotExistingEntityException, FailedOperationException, MalformattedElementException {
		String nsInstanceId = request.getNsInstanceId();
		log.debug("Building HTTP request for terminating NS instance " + nsInstanceId);
		
		HttpHeaders header = new HttpHeaders();
		header.add("Content-Type", "application/json");
		HttpEntity<?> putEntity = new HttpEntity<>(request, header);

		String url = nsLifecycleServiceUrl + "/ns/" + nsInstanceId + "/terminate";
		
		try {
			log.debug("Sending HTTP request to terminate NS.");
			ResponseEntity<String> httpResponse =
					restTemplate.exchange(url, HttpMethod.PUT, putEntity, String.class);
			
			log.debug("Response code: " + httpResponse.getStatusCode().toString());
			HttpStatus code = httpResponse.getStatusCode();
			
			if (code.equals(HttpStatus.OK)) {
				String operationId = httpResponse.getBody();
				log.debug("Started NS termination at NFVO. Operation ID: " + operationId);
				return operationId;
			} else if (code.equals(HttpStatus.NOT_FOUND)) {
				throw new NotExistingEntityException("Error during NS termination at NFVO: " + httpResponse.getBody());
			} else if (code.equals(HttpStatus.BAD_REQUEST)) {
				throw new MalformattedElementException("Error during NS termination at NFVO: " + httpResponse.getBody());
			} else {
				throw new FailedOperationException("Generic error on NFVO during NS termination");
			}
			
		} catch (RestClientException e) {
			log.debug("Error while interacting with NFVO.");
			throw new FailedOperationException("Error while interacting with NFVO LCM at url " + url);
		}
	}
	
	public void deleteNsIdentifier(String nsInstanceIdentifier)
			throws MethodNotImplementedException, NotExistingEntityException, FailedOperationException {
		log.debug("Building HTTP request for deleting NS instance identifier " + nsInstanceIdentifier);

		HttpHeaders header = new HttpHeaders();
		header.add("Content-Type", "application/json");
		HttpEntity<?> deleteEntity = new HttpEntity<>(header);

		String url = nsLifecycleServiceUrl + "/ns/" + nsInstanceIdentifier;
		
		try {
			log.debug("Sending HTTP request to delete NS ID.");
			ResponseEntity<String> httpResponse =
					restTemplate.exchange(url, HttpMethod.DELETE, deleteEntity, String.class);
			
			log.debug("Response code: " + httpResponse.getStatusCode().toString());
			HttpStatus code = httpResponse.getStatusCode();
			
			if (code.equals(HttpStatus.OK)) {
				log.debug("NS ID removed at NFVO.");
				return;
			} else if (code.equals(HttpStatus.NOT_FOUND)) {
				throw new NotExistingEntityException("Error during NS ID removal at NFVO: " + httpResponse.getBody());
			} else {
				throw new FailedOperationException("Generic error on NFVO during NS ID removal");
			}
			
		} catch (RestClientException e) {
			log.debug("Error while interacting with NFVO.");
			throw new FailedOperationException("Error while interacting with NFVO LCM at url " + url);
		}
	}
	
	public OperationStatus getOperationStatus(String operationId) throws NotExistingEntityException, FailedOperationException, MalformattedElementException {
		log.debug("Building HTTP request to retrieve status for NS LCM operation " + operationId);

		HttpHeaders header = new HttpHeaders();
		header.add("Content-Type", "application/json");
		HttpEntity<?> getEntity = new HttpEntity<>(header);

		String url = nsLifecycleServiceUrl + "/operation/" + operationId;

		try {
			log.debug("Sending HTTP request to retrieve operation status.");
			ResponseEntity<String> httpResponse =
					restTemplate.exchange(url, HttpMethod.GET, getEntity, String.class);

			log.debug("Response code: " + httpResponse.getStatusCode().toString());
			HttpStatus code = httpResponse.getStatusCode();

			if (code.equals(HttpStatus.OK)) {
				log.debug("Operation status correctly retrieved.");
				String operationStatus = httpResponse.getBody();
				
				if (operationStatus.equals("PROCESSING")) return OperationStatus.PROCESSING;
				else if (operationStatus.equals("SUCCESSFULLY_DONE")) return OperationStatus.SUCCESSFULLY_DONE;
				else if (operationStatus.equals("FAILED")) return OperationStatus.FAILED;
				else {
					log.error("Unknown operation status returned.");
					throw new FailedOperationException("Unknown operation status returned.");
				}
			} else if (code.equals(HttpStatus.NOT_FOUND)) {
				throw new NotExistingEntityException("Error during retrieval of operation status at NFVO: " + httpResponse.getBody());
			} else if (code.equals(HttpStatus.BAD_REQUEST)) {
				throw new MalformattedElementException("Error during retrieval of operation status at NFVO: " + httpResponse.getBody());
			} else {
				throw new FailedOperationException("Generic error on NFVO during retrieval of operation status");
			}

		} catch (RestClientException e) {
			log.debug("Error while interacting with NFVO.");
			throw new FailedOperationException("Error while interacting with NFVO LCM at url " + url);
		}

	}
	
	public OperationStatus getConfigStatus(String operationId) throws NotExistingEntityException, FailedOperationException, MalformattedElementException {
		log.debug("Building HTTP request to retrieve the config status for NS LCM operation " + operationId);

		HttpHeaders header = new HttpHeaders();
		header.add("Content-Type", "application/json");
		HttpEntity<?> getEntity = new HttpEntity<>(header);

		String url = nsLifecycleServiceUrl + "/config/" + operationId;

		try {
			log.debug("Sending HTTP request to retrieve config status.");
			ResponseEntity<String> httpResponse =
					restTemplate.exchange(url, HttpMethod.GET, getEntity, String.class);

			log.debug("Response code: " + httpResponse.getStatusCode().toString());
			HttpStatus code = httpResponse.getStatusCode();

			if (code.equals(HttpStatus.OK)) {
				log.debug("Config status correctly retrieved.");
				String configStatus = httpResponse.getBody();
				
				if (configStatus.equals("PROCESSING")) return OperationStatus.PROCESSING;
				else if (configStatus.equals("SUCCESSFULLY_DONE")) return OperationStatus.SUCCESSFULLY_DONE;
				else if (configStatus.equals("FAILED")) return OperationStatus.FAILED;
				else {
					log.error("Unknown operation status returned.");
					throw new FailedOperationException("Unknown operation status returned.");
				}
			} else if (code.equals(HttpStatus.NOT_FOUND)) {
				throw new NotExistingEntityException("Error during retrieval of operation status at NFVO: " + httpResponse.getBody());
			} else if (code.equals(HttpStatus.BAD_REQUEST)) {
				throw new MalformattedElementException("Error during retrieval of operation status at NFVO: " + httpResponse.getBody());
			} else {
				throw new FailedOperationException("Generic error on NFVO during retrieval of operation status");
			}

		} catch (RestClientException e) {
			log.debug("Error while interacting with NFVO.");
			throw new FailedOperationException("Error while interacting with NFVO LCM at url " + url);
		}

	}
	
	//******************************** NSD methods ********************************//
	
	public QueryNsdResponse queryNsd(GeneralizedQueryRequest request) throws MalformattedElementException, NotExistingEntityException, FailedOperationException {
		
		log.debug("Building HTTP request to query NSD.");
		HttpHeaders header = new HttpHeaders();
		header.add("Content-Type", "application/json");
		HttpEntity<?> postEntity = new HttpEntity<>(request, header);
		
		String url = nsdCatalogueServiceUrl + "/nsd/query";
		
		try {
			log.debug("Sending HTTP request to retrieve NSD.");
			
			ResponseEntity<QueryNsdResponse> httpResponse =
					restTemplate.exchange(url, HttpMethod.POST, postEntity, QueryNsdResponse.class);
			
			log.debug("Response code: " + httpResponse.getStatusCode().toString());
			HttpStatus code = httpResponse.getStatusCode();
			
			if (code.equals(HttpStatus.OK)) {
				log.debug("NSD correctly retrieved");
				return httpResponse.getBody();
			} else if (code.equals(HttpStatus.NOT_FOUND)) {
				throw new NotExistingEntityException("Error during NSD retrieval at NFVO: " + httpResponse.getBody());
			} else if (code.equals(HttpStatus.BAD_REQUEST)) {
				throw new MalformattedElementException("Error during NSD retrieval at NFVO: " + httpResponse.getBody());
			} else {
				throw new FailedOperationException("Generic error on NFVO during NSD retrieval");
			}
			
		} catch (RestClientException e) {
			log.debug("error while interacting with NFVO.");
			throw new FailedOperationException("Error while interacting with NFVO NSD catalogue at url " + url);
		}
	}
	
	//******************************** VNF package methods ********************************//
	
	public QueryOnBoardedVnfPkgInfoResponse queryVnfPackageInfo(GeneralizedQueryRequest request) throws NotExistingEntityException, MalformattedElementException {
		log.debug("Building HTTP request to query VNF package.");
		HttpHeaders header = new HttpHeaders();
		header.add("Content-Type", "application/json");
		HttpEntity<?> postEntity = new HttpEntity<>(request, header);
		
		String url = vnfdCatalogueServiceUrl + "/vnfPackage/query";
		
		try {
			log.debug("Sending HTTP request to retrieve VNF Package.");
			
			ResponseEntity<QueryOnBoardedVnfPkgInfoResponse> httpResponse =
					restTemplate.exchange(url, HttpMethod.POST, postEntity, QueryOnBoardedVnfPkgInfoResponse.class);
			
			log.debug("Response code: " + httpResponse.getStatusCode().toString());
			HttpStatus code = httpResponse.getStatusCode();
			
			if (code.equals(HttpStatus.OK)) {
				log.debug("VNF package correctly retrieved");
				return httpResponse.getBody();
			} else if (code.equals(HttpStatus.NOT_FOUND)) {
				throw new NotExistingEntityException("Error during VNF package retrieval at NFVO: " + httpResponse.getBody());
			} else if (code.equals(HttpStatus.BAD_REQUEST)) {
				throw new MalformattedElementException("Error during VNF package retrieval at NFVO: " + httpResponse.getBody());
			} else {
				throw new NotExistingEntityException("Generic error during VNF package retrieval at NFVO: " + httpResponse.getBody());
			}
			
		} catch (RestClientException e) {
			log.debug("error while interacting with NFVO.");
			throw new NotExistingEntityException("Error while interacting with NFVO VNFD catalogue at url " + url);
		}
	}
}
