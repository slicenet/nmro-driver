# Network Domain Resource Orchestrator Driver for SliceNet Project

NMR-O Connector for the SliceNet Slice Orchestrator (SS-O) at NSP level.


## Overview  

Once installed, this driver allows the SS-O to transparently manage and configure the NSP infrastrucutre
orchestrated by the NMR-O.   

## Build

To compile the library use the command:

$ mvn compile

To package the library into a jar file use the command:

$ mvn package

To install the library in the local repository use the command:

$ mvn install

